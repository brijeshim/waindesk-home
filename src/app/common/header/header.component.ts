import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  mobileQuery: MediaQueryList;
  isSmallScreen: boolean;
  breakpoint: any;
  private _mobileQueryListener: () => void;
  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, 
    public breakpointObserver: BreakpointObserver) {
      this.mobileQuery = media.matchMedia('(max-width: 959.99px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
      // this.breakpointObserver.observe([
      //   Breakpoints.XSmall,
      //   Breakpoints.Small,
      //   Breakpoints.Medium,
      //   Breakpoints.Large,
      //   Breakpoints.XLarge
      // ]).subscribe( (state: BreakpointState) => {
      //   if (state.breakpoints[Breakpoints.XSmall]) {
      //     this.breakpoint = Breakpoints.XSmall;
      //     this.isSmallScreen = true;
      //     console.log(this.breakpoint)
      //     this.mobileQuery = media.matchMedia('(max-width: 559.99px)');
      //     this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      //     this.mobileQuery.addListener(this._mobileQueryListener);
      //   } else if (state.breakpoints[Breakpoints.Small]) {
      //     this.breakpoint = Breakpoints.Small;
          
      //   }
        //  else if (state.breakpoints[Breakpoints.Medium]) {
        //   this.breakpoint = Breakpoints.Medium;
        //   this.mobileQuery = media.matchMedia("(max-width: 1279.99px)");
        //   this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        //   this.mobileQuery.addListener(this._mobileQueryListener);
        // }
        //  else {
        //   this.breakpoint = Breakpoints.Large;
        //   this.mobileQuery = media.matchMedia(this.breakpoint);
        //   this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        //   this.mobileQuery.addListener(this._mobileQueryListener);
        // }
      // })
    
   }

  ngOnInit(): void {
  }
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

}
