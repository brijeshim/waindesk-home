FROM node:14.19.0 as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod
#stage 2
FROM nginx:alpine
COPY --from=node /app/dist/waindesk /usr/share/nginx/html
COPY ./nginx /etc/nginx/conf.d